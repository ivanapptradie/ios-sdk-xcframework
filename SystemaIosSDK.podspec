#
#  Be sure to run `pod spec lint SystemaIos.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|
  spec.name         = "SystemaIosSDK"
  spec.version      = "0.1.0.10"
  spec.summary      = "Systema AI Mobile SDK"
  spec.description  = <<-DESC
  Systema AI Mobile SDK lets you easily integrate the Systema AI REST APIs with your mobile apps.
  Developer documentation = https://systema-labs.gitlab.io/systema-sdk/
    DESC
  spec.homepage     = "https://gitlab.com/systema-labs/systema-sdk-xcframework"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "The Systema AI Team" => "support@systema.ai" }
  spec.ios.deployment_target = "13.6"
  spec.source       = { :git => "https://gitlab.com/ivanapptradie/ios-sdk-xcframework.git", :tag => "#{spec.version}" }
  spec.exclude_files = "Classes/Exclude"
  spec.vendored_frameworks      = "SystemaIosSDK.xcframework"
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
